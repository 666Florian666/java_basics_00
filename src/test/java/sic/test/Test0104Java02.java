/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java02
 *
 * Class `Test0104Java02`, defined in its file `Test0104Java02.java`.
 *
 * Actually `Test0104Java02.java` exists as `sic/test/Test0104Java02.java` on your filesystem,
 * because class `Test0104Java02` is defined as a member of package `sic.test`.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java02 {
    /**
     * Global variable
     * - always exists (lifecycle)
     * - try to avoid (bad style)
     */
    public static int global_variable = 0;

    /**
     * Member variable of an instance of type Test0104Java01.
     * - Exists as long as its instance of type Test0104Java01.
     */
    int member_of_test0104java01_instance_variable = 0;
}
