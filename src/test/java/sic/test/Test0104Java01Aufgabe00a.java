/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import sic.test.util.SimpleJunit5Launcher;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Basic Java expressions and statements, part 0a.
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test0104Java01Aufgabe00a {
    @Test
    void test00() {
        // dummy
    }

    // @Test
    void test02_literals() {
        {
            final int exp = 2; // don't touch!
            final int i = 5;   // don't touch!
            final int has = i - 3; // FIXED
            Assertions.assertEquals(exp, has);
        }
        {
            final byte exp = (byte)85; // don't touch!
            // 85 - 64 = 21
            // 21 - 16 =  5
            //  5 -  4 =  1
            //  1 -  1 =  0 (Jonas Kretzer)
            final byte has = 0b01010101;   // FIXED _and_ stick using a binary literal
            Assertions.assertEquals(exp, has);
        }
        {
            final short exp = (short)45054; // don't touch!
            final short has = (short)0x1234; // FIXME _and_ stick using a hexadecimal literal
            Assertions.assertEquals(exp, has);
        }
        {
            final char exp = (char)0x30; // don't touch!
            final char has = 'C'; // FIXME _and_ stick using a char literal
            Assertions.assertEquals(exp, has);
        }
    }

    // @Test
    void test04_block_statement() {
        {
            int i = 2; // don't touch!
            Assertions.assertEquals(1, i); // FIXME

            {
                final int j = 6; // don't touch!
                final int k = i + j; // FIXME
                Assertions.assertEquals(3, k); // don't touch!

                i = i + 1; // don't touch!
            }
            Assertions.assertEquals(2, i); // FIXME
        }

    }

    // @Test
    void test10_basic_integer_arithmetik() {
        {
            Assertions.assertEquals(6, 1+2); // FIXME
        }
        {
            final int i = 6; // don't touch!
            final int j = 2; // don't touch!
            final int k = i + j; // FIXME
            Assertions.assertEquals(12, k); // don't touch!
        }
        {
            final int i = +6; // don't touch!
            final int j = -2; // don't touch!
            final int k = i + j; // FIXME
            Assertions.assertEquals(8, k); // don't touch!
        }
        {
            final int i = 7; // don't touch!
            final int j = 2; // don't touch!
            final int k = i / j; // FIXME
            Assertions.assertEquals(5, k); // don't touch!
        }
        {
            final int i = 3; // don't touch!
            final int j = 7; // don't touch!
            final int k = i - j; // FIXME
            Assertions.assertEquals(21, k); // don't touch!
        }
        {
            final int i = 64; // don't touch!
            final int j = 4; // don't touch!
            final int k = i + j; // FIXME
            Assertions.assertEquals(16, k); // don't touch!
        }
        {
            final int i = 6; // FIXME
            final int j = 2; // FIXME
            final int k = i % j; // don't touch!
            Assertions.assertEquals(2, k); // don't touch!
        }
        {
            final int i = 7; // don't touch!
            final int j = 2; // don't touch!
            final int k = i % j; // don't touch!
            Assertions.assertEquals(7, k); // FIXME
        }
    }

    // @Test
    void test11_unary_post_prefix() {
        {
            int i = 2; // don't touch!
            ++i; // don't touch!
            Assertions.assertEquals(8, i); // FIXME
            --i; // don't touch!
            Assertions.assertEquals(8, i); // FIXME

            // !!!!
            Assertions.assertEquals(8, i); // FIXME
            Assertions.assertEquals(8, ++i); // FIXME
            Assertions.assertEquals(8, i); // FIXME
        }

        {
            int i = 8; // FIXME
            i++; // don't touch!
            Assertions.assertEquals(7, i); // don't touch!
            i--; // FIXME ???
            Assertions.assertEquals(6, i);

            // !!!!
            Assertions.assertEquals(8, i); // FIXME
            Assertions.assertEquals(8, i++); // FIXME
            Assertions.assertEquals(8, i); // FIXME
        }
    }

    // @Test
    void test12_assignment() {
        {
            int i = 6; // don't touch!
            Assertions.assertEquals(8, i); // FIXME
            i = 1; // FIXME
            Assertions.assertEquals(2, i); // don't touch!
        }

        {
            int i = 8; // don't touch!
            i += 4; // FIXME
            Assertions.assertEquals(10, i); // don't touch!
        }
        {
            int i = 9; // don't touch!
            i -= 4; // FIXME
            Assertions.assertEquals(2, i); // don't touch!
        }
        {
            int i = 3; // don't touch!
            i *= 2; // FIXME
            Assertions.assertEquals(12, i); // don't touch!
        }
        {
            int i = 128; // don't touch!
            i /= 1; // FIXME
            Assertions.assertEquals(32, i); // don't touch!
        }
        {
            {
                int i = 6; // don't touch!
                i %= 1; // FIXME
                Assertions.assertEquals(0, i); // don't touch!
            }
            {
                int i = 5; // don't touch!
                i %= 8; // FIXME
                Assertions.assertEquals(1, i); // don't touch!
            }
        }
    }

    // @Test
    void test13_equality() {
        {
            final int i = 6; // don't touch!
            final int j = 6; // don't touch!
            final int k = 3; // don't touch!
            Assertions.assertEquals(true,  i != j); // FIXME
            Assertions.assertEquals(false, i == j); // FIXME

            Assertions.assertEquals(false, i != k); // FIXME
            Assertions.assertEquals(true,  i == k); // FIXME
        }
        {
            final int i = 7; // FIXME
            final int j = 6; // FIXME
            final int k = 5; // FIXME
            Assertions.assertEquals(false, i <  j); // don't touch!
            Assertions.assertEquals(true,  i <= j); // don't touch!
            Assertions.assertEquals(true,  i >= j); // don't touch!
            Assertions.assertEquals(false, i >  j); // don't touch!

            Assertions.assertEquals(true,  i <  k); // don't touch!
            Assertions.assertEquals(true,  i <= k); // don't touch!
            Assertions.assertEquals(false, i >= k); // don't touch!
            Assertions.assertEquals(false, i >  k); // don't touch!
        }
    }

    // @Test
    void test14_logic_boolean() {
        {
            final int i = 5; // FIXME
            final int j = 6; // FIXME
            final int k = 7; // FIXME
            final boolean b0 = i==j; // don't touch!
            final boolean b1 = i==k; // don't touch!

            // De Morgan’s Law
            //
            // ¬(A ∧ B) = ¬A ∨ ¬B
            //
            // true  == b0 && !b1
            // false == !(b0 && !b1) == !b0 || b1
            //
            Assertions.assertEquals(true,   b0 && !b1);      // don't touch!
            Assertions.assertEquals(false,  !( b0 && !b1) ); // don't touch!
            Assertions.assertEquals(false,  !b0 ||  b1);     // don't touch!

            Assertions.assertEquals(false,  i != j || i == k); // don't touch!
            Assertions.assertEquals(true,   i == j && i != k); // don't touch!
        }
    }

    public static void main(final String[] args) {
        SimpleJunit5Launcher.runTests(Test0104Java01Aufgabe00a.class);
    }

}
